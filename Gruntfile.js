/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),

    jade: {
      compile: {
        options: {
          pretty: true,
          data: {
            jobs: grunt.file.readJSON('data.json')
          }
        },
        files: {
          'public/index.html':'jade/jobs.jade'
        }
      }
    },

    sass: {  
      options: {
        includePaths: ['public/components/foundation/scss']
      },
      dist: {
        options: {
          sourceMap: false
        },
        files: {
          'public/css/app.css': 'assets/styles/scss/app.scss'
        }
      }
    },
    watch: {
      grunt: { files: ['Gruntfile.js'] },

      sass: {
        files: 'assets/styles/scss/*.scss',
        tasks: ['sass']
      },
      jade: {
        files: ['jade/*.jade','jade/**/*.jade'],
        tasks: ['jade']
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-sass');

  // Default task.
  grunt.registerTask('build', ['sass', 'jade']);
  grunt.registerTask('default','default', ['build', 'watch', 'jade', 'sass']);
  

};