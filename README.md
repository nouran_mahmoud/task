### Install dependancies
> `npm install`
dependancies are:
1- grunt
2- jade
3- sass
4- watch


### Run Bower for installing foundation
> `bower install`

### Run Grunt task
> `grunt`
tasks are:
1- jade
2- sass
3- watch
4- default for [jade, sass, watch]

### Run rack webserver
> `rackup` and open `localhost:9292`